import time
import undetected_chromedriver as driver
from selenium import webdriver
from selenium.common.exceptions import (
    TimeoutException,
    ElementNotInteractableException,
)
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager
import pyotp


totp = pyotp.TOTP("6czoivb7ttklzczlo5aitkli2iembxrj")

options = Options()
options.add_argument('--window-size=1920,1080')
options.add_argument('--headless=new')
options.add_argument("--incognito")
options.add_argument('--user-data-dir=/tmp/profile')

browser = driver.Chrome(options=options)
browser.delete_all_cookies()
browser  = webdriver.Chrome(ChromeDriverManager().install(), options=options)
browser.get('https://www.google.com/maps/place/Kem+Tr%C3%A0ng+Ti%E1%BB%81n+s%E1%BB%91+1+H%C3%A0+N%E1%BB%99i+chi+nh%C3%A1nh+Th%E1%BB%91ng+Nh%E1%BA%A5t/@10.8376399,106.5209109,12z/data=!4m10!1m2!2m1!1skem!3m6!1s0x31752903990c50b1:0xbebe692668e9d859!8m2!3d10.8376399!4d106.6651065!15sCgNrZW1aBSIDa2VtkgEOaWNlX2NyZWFtX3Nob3DgAQA!16s%2Fg%2F11j2p3yvq_')


def get_element(driver, by: str = By.ID, value: str = None, timeout=30):
    try:
        WebDriverWait(driver, timeout=timeout).until(lambda d: d.find_element(by, value))
    except TimeoutException as e:
        browser.get_screenshot_as_file(f"debug-{time.time()}.png")
        print(e)
        raise Exception() from e
    except ElementNotInteractableException as e:
        browser.get_screenshot_as_file(f"debug-{time.time()}.png")
        print(e)
        raise Exception() from e
    element = browser.find_element(by, value)
    return element

    
# WebDriverWait(browser, timeout=30).until(lambda d: d.find_element(By.XPATH, '//*[@id="gb"]/div/div/div[1]/div[2]'))
# browser.find_element(By.XPATH, '//*[@id="gb"]/div/div/div[1]/div[2]').click()

button_signin = get_element(browser, By.XPATH, '//*[@id="gb"]/div/div/div[1]/div[2]')
button_signin.click()

# Login
print('login')
# get_element(browser, By.XPATH, '//*[@id="identifierId"]').send_keys('xk3newsound@gmail.com', Keys.ENTER)
# get_element(browser, By.XPATH, '//*[@id="password"]/div[1]/div/div[1]/input').send_keys('lonelyinwood', Keys.ENTER)
# # Use google authenticator
# get_element(browser, By.XPATH, '//*[@id="yDmH0d"]/c-wiz/div/div[2]/div/div[2]/div[2]/div[2]/div/div/button').click()
# get_element(browser, By.XPATH, '//*[@id="view_container"]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[1]/ul/li[3]/div').click()
# get_element(browser, By.XPATH, '//*[@id="totpPin"]').send_keys(totp.now(), Keys.ENTER)


# # Ratting
# get_element(browser, By.XPATH, '//*[@id="QA0Szd"]/div/div/div[1]/div[3]/div/div[1]/div/div/div[2]/div[3]/div/div/button[2]').click()
# get_element(browser, By.XPATH, '//*[@id="QA0Szd"]/div/div/div[1]/div[3]/div/div[1]/div/div/div[3]/div[4]/div/button').click()
# get_element(browser, By.XPATH, '//*[@id="kCvOeb"]/div[1]/div[3]/div/div[2]/div/div[5]').click()
# get_element(browser, By.XPATH, '//*[@id="kCvOeb"]/div[2]/div/div[2]/div/button/div[3]').click()



# if 'local guide' in get_element(browser, By.XPATH, '//*[@id="thank-you-title"]/div[1]').text.lower():
#     get_element(browser, By.XPATH, '//*[@id="yDmH0d"]/c-wiz/div/div/div/c-wiz/div/div[2]/div/div[1]/div/button/div[3]').click()
# else:
#     browser.get_screenshot_as_file("debug.png")

