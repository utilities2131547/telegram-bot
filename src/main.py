from telethon import TelegramClient
from telethon import utils
from telethon import types

# These example values won't work. You must get your own api_id and
# api_hash from https://my.telegram.org, under API Development.
api_id = 29418737
api_hash = 'd0a4ea639c564bfc9370bac8eb46a53d'

real_id, peer_type = utils.resolve_id(-1856588225)

client = TelegramClient('session_name', api_id, api_hash)
# @client.on(events.NewMessage(chats=real_id, from_users=real_id))
# async def my_event_handler(event):
#     print(event.raw_text)


# from telethon.tl.functions.messages import GetHistoryRequest
# from telethon.tl.types import PeerChannel
client.connect()
channel_entity = client.get_entity(types.PeerChannel(real_id))
for message in client.iter_messages(channel_entity, filter=types.InputMessagesFilterPinned()):
    print(message.to_dict().get('message'))